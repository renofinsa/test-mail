FROM node:13

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
VOLUME ["/public/"]

# ADD . /path/inside/docker/container
COPY package*.json ./
COPY .env.example .env

# RUN npm cache clean --force && npm install -g npm@latest --force && npm cache clean --force && rm -rf node_modules package-lock.json && npm install && npm uninstall bcrypt && npm install bcrypt  --save && npm install -g pm2
RUN npm install -g pm2 && npm install && npm uninstall bcrypt && npm install bcrypt  --save 

COPY . .

EXPOSE 3001

CMD [ "pm2-runtime", "--env", "development", "ecosystem.config.json" ]
