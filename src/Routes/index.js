const app = require('express').Router();
const { qrCode, sendMail } = require('./../Helpers/Generator');

/** Write ur routes here */
app.get('/', async (req, res) => {
  // const QRCode = require('qrcode');
  // let data = await QRCode.toDataURL(`http://56-entertainment.com/concert/saraingiraneun-ireumeuro/check-in/${req.params.id}`, {type:'terminal'});

  // console.log(data);

  return res.success({ message: 'Email has been sent', data: "" });
})

app.post('/mail', async (req, res) => {
  let imageQr = await qrCode(req.body.id);

  let response = await sendMail({
    name: req.body.name,
    email: req.body.email,
    barcode: imageQr,
    categoryTicket: req.body.categoryTicket,
    qty: req.body.qty
  });
  
  return res.success({ message: 'Email has been sent', data: response });
});

module.exports = app