const fs = require('fs')
const QRCode = require('qrcode');
const SibApiV3Sdk = require('sib-api-v3-sdk');
const defaultClient = SibApiV3Sdk.ApiClient.instance;

exports.qrCode = async (id) => {
  try {
    let response = await QRCode.toString(`http://56-entertainment.com/concert/saraingiraneun-ireumeuro/check-in/${id}`)
    return response;
  } catch (err) {
    console.error(err)
  }
};

exports.sendMail = async (payload) => {
  try {

    // Configure API key authorization: api-key
    var apiKey = defaultClient.authentications['api-key'];
    apiKey.apiKey = process.env.SEND_IN_BLUE_API;

    // Uncomment below two lines to configure authorization using: partner-key
    // var partnerKey = defaultClient.authentications['partner-key'];
    // partnerKey.apiKey = 'YOUR API KEY';

    var apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();

    var sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail(); // SendSmtpEmail | Values to send a transactional email

    sendSmtpEmail = {
      sender:{
        email:"support@56-entertainment.com",
        name:"Admin Support"
        },
        to: [{
            email: payload.email,
            name: payload.name
        }],
        subject:"Konfirmasi Pemesanan Tiket - Sarang Iraneun Ireumeuro",
        htmlContent: `
          <!DOCTYPE html>
            <html>
            <head></head>
            <body>
              <p>Dear ${payload.name}, </p>
              <p>
                Terima kasih telah memesan tiket konser Sarang Iraneun Ireumeuro di Jakarta (17 Desember 2022)
                <br />
                Melalui email ini, kami informasikan bahwa pemesanan tiket anda sedang kami proses, dalam waktu 1x24 jam!
              </p>
              <p>Berikut detail pemesanan anda:</p>
              <ul>
                <li>Event Name : Sarang Iraneun Ireumeuro</li>
                <li>Event Location : Hall B3-C3 Jiexpo, Kemayoran</li>
                <li>Event Schedule  : Saturday, 17 December 2022</li>
                <li>Category : ${payload.categoryTicket}</li>
                <li>Quantity : ${payload.qty}</li>
              </ul>
            </body>
          </html>
        `,
        // attachment: [{
        //   // url: `${payload.barcode}`,
        //   name: "file.png",
        //   content: payload.barcode
        // }]
      };
      // [{"url":"https://attachment.domain.com/myAttachmentFromUrl.jpg", "name":"myAttachmentFromUrl.jpg"}, {"content":"base64 example content", "name":"myAttachmentFromBase64.jpg"}].

    apiInstance.sendTransacEmail(sendSmtpEmail).then(function(data) {
      console.log('API called successfully. Returned data: ' + sendSmtpEmail.htmlContent);
    }, function(error) {
      console.error(error);
    });
  } catch (error) {
    console.log(error);
  }
}
