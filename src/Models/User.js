// SEQUELIZE
const Database = require('./../Database');
const { User } = Database;
const sequelize = Database.Sequelize;
const { Op } = sequelize;

const bcrypt = require('bcrypt')

exports.get = async ({ userId = null }) => {
  try {
    if (!userId) {
      var data = await User.findAll({
        attributes: ['id', 'name', 'email', 'phoneNumber', 'createdAt', 'updatedAt'],
        order: [['id', 'DESC']]
      });
      
      return data;
    }

    var data = await User.findOne({
      where: {id: userId},
      attributes: ['id', 'name', 'email', 'phoneNumber', 'createdAt', 'updatedAt']
    });
    
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.getByEmail = async ({ email, userId = null }) => {
  try {
    var data = await User.findOne({
      where: {
        email: email,
        id: {[Op.ne]: userId}
      },
      attributes: ['id', 'name', 'email', 'phoneNumber', 'createdAt']
    });
    
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.store = async (req) => {
  try {
    const {
      name,
      email,
      password,
      phoneNumber
    } = req.body;

    const data = await User.create({
      name: name,
      email: email,
      password: bcrypt.hashSync(password, 12),
      phoneNumber: phoneNumber
    });

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

exports.update = async (req) => {
  try {
    const {
      name,
      email,
      password,
      phoneNumber
    } = req.body;

    const data = await User.update(
      {
        name: name,
        email: email,
        password: password,
        phoneNumber: phoneNumber
      }, 
      { where: { id: req.params.id } }
    );

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

exports.destroy = async (req) => {
  try {
    const data = await User.findOne({ where: {id: req.params.id} });
    
    if (data) {
      data.destroy();
      return true;
    }

    return false;
  } catch (error) {
    console.log(error);
    throw error;
  }
}